/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Abs extends operator1arg {
    public Abs(wyrazenie wyr1){
        this.wyr1 = wyr1;
    }
    
    public Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException {
        if (wyr1.oblicz().mode == "macierz" || wyr1.oblicz().mode == "wektor")
            throw new WrongTypeException();
        
        try{
            if (wyr1.oblicz().rzeczywista < 0)
                return new Wartosc(wyr1.oblicz().rzeczywista * (-1));
        }
        catch (WrongTypeException e){
            throw e;
        }
        catch(Div0Exception e){
            throw e;
        }
        catch(Macierz.WrongSizeException e){
            throw e;
        }
        catch(UndefinedVariableException e){
            throw e;
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        return new Wartosc(wyr1.oblicz().rzeczywista);
    }
    
    public String opis(){
        return "abs(" + wyr1.opis() + ")";
    }
}
