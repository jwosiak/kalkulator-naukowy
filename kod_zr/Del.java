/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Del extends operator1arg {
    public Del(wyrazenie wyr1){
        this.wyr1 = wyr1;
    }
    
    public Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException {
        try{
            wyr1.oblicz();
            if (wyr1 instanceof Zmienna){
                return (new Zmienna(wyr1.opis()).usun_zm());
            }
        }
        catch (WrongTypeException e){
            throw e;
        }
        catch(Div0Exception e){
            throw e;
        }
        catch(Macierz.WrongSizeException e){
            throw e;
        }
        catch(UndefinedVariableException e){
            throw e;
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        throw new UndefinedVariableException();
    }
    
    public String opis(){
        return "del(" + wyr1.opis() + ")";
    }
}
