/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Dziel extends operator2arg {
    public Dziel(wyrazenie wyr1, wyrazenie wyr2){
        this.wyr1 = wyr1;
        this.wyr2 = wyr2;
    }
    
    public Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException {
        try{
            if (wyr2.oblicz().mode == "rzeczywista" && wyr2.oblicz().rzeczywista == 0.0 ||
                wyr2.oblicz().mode == "macierz" && wyr2.oblicz().macierz.det() == 0.0    )
                throw new Div0Exception();
            
            if (wyr1.oblicz().mode == "rzeczywista" && wyr2.oblicz().mode == "rzeczywista")
                return new Wartosc((wyr1.oblicz()).rzeczywista / (wyr2.oblicz()).rzeczywista);
            
//            if (wyr1.oblicz().mode == "wektor" && wyr2.oblicz().mode == "wektor")
//                return new Wartosc((wyr1.oblicz()).wektor.mnoz((wyr2.oblicz()).wektor));            
            
            if (wyr1.oblicz().mode == "macierz" && wyr2.oblicz().mode == "macierz")
                return new Wartosc((wyr1.oblicz()).macierz.mnoz((wyr2.oblicz()).macierz.odwrotna()));
            
            if (wyr1.oblicz().mode == "macierz" && wyr2.oblicz().mode == "rzeczywista")
                return new Wartosc((wyr1.oblicz()).macierz.mnoz((1/(wyr2.oblicz()).rzeczywista)));
            
            if (wyr1.oblicz().mode == "wektor" && wyr2.oblicz().mode == "rzeczywista")
                return new Wartosc((wyr1.oblicz()).wektor.mnoz((1/(wyr2.oblicz()).rzeczywista)));
            }
        catch(WrongTypeException e){
            throw e;
        }
        catch(Macierz.WrongSizeException e){
            throw e;
        }
        catch(UndefinedVariableException e){
            throw e;
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        throw new WrongTypeException();
    }
    
    public String opis(){
        return wyr1.opis() + "*" + wyr2.opis();
    }
}
