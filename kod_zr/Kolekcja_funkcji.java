/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 *
 * @author Jakub
 */
public class Kolekcja_funkcji {
    public LinkedList<String> n_fun1;
    public LinkedList<String> n_fun2;
    public Hashtable<String,Integer> op;
    
    public Kolekcja_funkcji(){
        n_fun1 = new LinkedList<>();
        n_fun1.add("abs");
        n_fun1.add("!");
        n_fun1.add("det");
        n_fun1.add("tr");
        n_fun1.add("del");
        
        n_fun2 = new LinkedList<>();
        n_fun2.add("min");
        n_fun2.add("max");
        
        op = new Hashtable<>();
        op.put("=",11);     ///operatory o parzystym priorytecie łącza w lewo; o nieparzystym w prawo
        op.put("+",6);
        op.put("-",6);
        op.put("*",4);
        op.put("/",4);
        op.put("^",3);
        op.put("!",0);
    }
    
    public operator1arg funkcja1arg(String nazwa, wyrazenie arg1) {
        switch (nazwa){
            case "abs": return new Abs(arg1);
            case "!": return new Negacja(arg1);
            case "det": return new Det(arg1);
            case "tr": return new Trans(arg1);
            case "del": return new Del(arg1);
        }
            
        return null;
    }
    
    public operator2arg funkcja2arg(String nazwa, wyrazenie arg1, wyrazenie arg2) {
        switch (nazwa){
            case "+": return new Dodaj(arg1,arg2);
            case "-": return new Odejmij(arg1,arg2);
            case "*": return new Mnoz(arg1,arg2);
            case "/": return new Dziel(arg1,arg2);
            case "=": return new Rownosc(arg1,arg2);
            case "^": return new Pot(arg1,arg2);
            case "min": return new Min(arg1,arg2);
            case "max": return new Max(arg1,arg2);
        
        }
        return null;
    }
    
    /*
    class UnidentifiedFunctionException extends Exception{
        public UnidentifiedFunctionException(){
            super();
        }
        
        public String info(){
            return "nieznana nazwa funkcji";
        }
    }
    */
}
