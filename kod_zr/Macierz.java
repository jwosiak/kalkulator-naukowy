/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Jakub
 */
public class Macierz {
    public Wektor w[];
    
    public Macierz(Wektor w[]){
        this.w = w;
    }
    
    public Macierz(List<Wektor> w){
        Wektor wek[] = new Wektor[w.size()];
        
        for (int i = 0; i < w.size(); i++){
            wek[i] = w.get(i);
        }
        
        this.w = wek;
    }
    
    private LinkedList<String> scanner(String wejscie){
        LinkedList<String> wektory = new LinkedList<>();
        
        for (int i = 0; i < wejscie.length()-1; i++){
            String wektor = "";
            for (int j = i; wejscie.charAt(j) != '}'; j++, i++){     ///przydalby sie wyjatek
                wektor += wejscie.charAt(j);
            }
            wektor += '}';
            wektory.add(wektor);
        }
        
        return wektory;
    }
    
    public Macierz(String wejscie){
        LinkedList<String> wektory = scanner(wejscie);
        
        this.w = new Wektor[wektory.size()];
        for (int i = 0; i < wektory.size(); i++){
            this.w[i] = new Wektor(wektory.get(i));
        }
    }
    
    public String toString(){
        String napis = new String();
        if (w.length == 0)
            return "";
        
        napis += "{";
        for (int i = 0; i < w.length; i++){
            napis += w[i].toString();
            if (i != w.length-1)
                napis += ", ";
        }
        napis += "}";
        return napis;
    }    
    
    public Macierz mnoz(Macierz m) throws WrongSizeException {
        if (w.length != m.w[0].wsp.length)
            throw new WrongSizeException();
            
        LinkedList<Wektor> nowa = new LinkedList<Wektor>();
        for (int i = 0; i < m.w.length; i++){
            double wsp[] = new double[w[0].wsp.length];
            for (int j = 0; j < this.w[0].wsp.length; j++){
                wsp[j] = 0;
                for (int k = 0; k < this.w.length; k++){
                    wsp[j] += w[k].wsp[j] * m.w[i].wsp[k];
                }
            }
            nowa.add(i, new Wektor (wsp));
        }
        return new Macierz(nowa);
    }
    
    public Macierz mnoz(double skalar){
        Wektor wek[] = new Wektor[this.w.length];
        for (int i = 0; i < wek.length; i++){
            wek[i] = this.w[i].mnoz(skalar);
        }
        
        return new Macierz(wek);
    }
    
    public Macierz dodaj(Macierz m) throws WrongSizeException, Wektor.WrongSizeException {
        if (w.length != m.w.length || w[0].wsp.length != m.w[0].wsp.length)
            throw new WrongSizeException();
        
        Wektor wek[] = new Wektor[this.w.length];
        try{
            for (int i = 0; i < wek.length; i++){
                wek[i] = this.w[i].dodaj(m.w[i]);
            }
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        return new Macierz(wek);
    }
    
    public Macierz odejmij(Macierz m) throws WrongSizeException, Wektor.WrongSizeException {
        if (w.length != m.w.length || w[0].wsp.length != m.w[0].wsp.length)
            throw new WrongSizeException();
        
        Wektor wek[] = new Wektor[this.w.length];
        try{
            for (int i = 0; i < wek.length; i++){
                wek[i] = this.w[i].odejmij(m.w[i]);
            }
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        return new Macierz(wek);
    }
    
    public Macierz minor (int i,int j){
        if (w.length <= i || w[0].wsp.length <= j)
            return this;
        
        Wektor wek[] = new Wektor[this.w.length-1];
        
        boolean za_wyrzuconym = false;
        for (int k = 0; k < w.length; k++){
            if (k == j){
                za_wyrzuconym = true;
                continue;
            }
            
            if (za_wyrzuconym == false){
                wek[k] = w[k];
            }
            else{
                wek[k-1] = w[k];
            }
        }
        
        for (int l = 0; l < wek.length; l++){
            double wsp[] = new double[w[0].wsp.length-1];
            za_wyrzuconym = false;
            for (int k = 0; k < wsp.length+1; k++){
                if (k == i){
                    za_wyrzuconym = true;
                    continue;
                }

                if (za_wyrzuconym == false){
                    wsp[k] = wek[l].wsp[k];
                }
                else{
                    wsp[k-1] = wek[l].wsp[k];
                }
            }
            wek[l] = new Wektor(wsp);
        }
        
        return new Macierz(wek);
    }
    
    public double det() throws WrongSizeException {
        if (w.length != w[0].wsp.length)
            throw new WrongSizeException();
            
        if (this.w.length == 1)
            return this.w[0].wsp[0];
        
        if (this.w.length == 2){
            return w[0].wsp[0]*w[1].wsp[1] - w[1].wsp[0]*w[0].wsp[1];
        }
        
        if (this.w.length == 3){    ///Reguła Sarrusa
            return w[0].wsp[0]*w[1].wsp[1]*w[2].wsp[2] + w[1].wsp[0]*w[2].wsp[1]*w[0].wsp[2] +
                    w[2].wsp[0]*w[0].wsp[1]*w[1].wsp[2] - w[2].wsp[0]*w[1].wsp[1]*w[0].wsp[2] -
                    w[0].wsp[0]*w[2].wsp[1]*w[1].wsp[2] - w[1].wsp[0]*w[0].wsp[1]*w[2].wsp[2];
        }
        
        double wyzn = 0;
        for (int i = 0; i < w.length; i++) //rozwinięcie Laplace'a
            wyzn += dop_alg(0,i) * this.w[i].wsp[0];
        
        
        return wyzn;
    }
    
    public Macierz transponuj(){
        Wektor wt[] = new Wektor[w[0].wsp.length];
        
        for (int i = 0; i < wt.length; i++){
            double wsp[] = new double[w.length];
            for (int j = 0; j < wt.length; j++){
                wsp[j] = w[j].wsp[i];
            }
            wt[i] = new Wektor(wsp);
        }
        
        return new Macierz(wt);
    }
    
    public double dop_alg(int i, int j) throws WrongSizeException {
        try{
            if ((i+j) % 2 == 0)
                return this.minor(i,j).det();
            else
                return this.minor(i,j).det() * (-1);
        }
        catch(WrongSizeException e){
            throw e;
        }
    }
    
    public Macierz m_dopelnien() throws WrongSizeException {
        try{
            Wektor wek[] = new Wektor[w.length];
            for (int i = 0; i < w.length; i++){
                double wsp[] = new double[w[0].wsp.length];
                for (int j = 0; j < w[0].wsp.length; j++){
                    wsp[j] = dop_alg(i,j);
                }
                wek[i] = new Wektor(wsp);
            }
            return new Macierz(wek);
        }
        catch(WrongSizeException e){
            throw e;
        }
    }
    
    public Macierz odwrotna() throws WrongSizeException {
        try{
            if (det() != 0)
                return (m_dopelnien()).mnoz(1/det());
        }
        catch(WrongSizeException e){
            throw e;
        }
        
        return null;
    }
    
    
    public Macierz pow(int wykl) throws WrongSizeException {
        if (w.length != w[0].wsp.length)
            throw new WrongSizeException();
        
        Wektor wek[] = new Wektor[w.length];
        for (int i = 0; i < w.length; i++){     ///macierz identycznosciowa
            double wsp[] = new double[w.length];
            for (int j = 0; j < w[0].wsp.length; j++){
                if (i == j)
                    wsp[j] = 1;
                else
                    wsp[j] = 0;
            }
            wek[i] = new Wektor(wsp);
        }
        Macierz rez = new Macierz(wek);
        if (wykl == 0)
            return rez;
        
        int b = wykl>=0? wykl:-wykl;
        Macierz a = this;
        while(b>0){
            if (b % 2 == 1)
                rez = rez.mnoz(a);
            
            b /= 2;
            a = a.mnoz(a);
        }
        if (wykl < 0){
            rez = rez.odwrotna();
        }

        return rez;
    }
    
    class WrongSizeException extends Exception{
        public WrongSizeException(){
            super();
        }
        
        public String info(){
            return "nieprawidłowy rozmiar macierzy";
        }
    }
}
