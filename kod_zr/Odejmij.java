/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Odejmij extends operator2arg {
    public Odejmij(wyrazenie wyr1, wyrazenie wyr2){
        this.wyr1 = wyr1;
        this.wyr2 = wyr2;
    }
    
    public Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException {
        try{
            if (wyr1.oblicz().mode == "rzeczywista" && wyr2.oblicz().mode == "rzeczywista")
                return new Wartosc((wyr1.oblicz()).rzeczywista - (wyr2.oblicz()).rzeczywista);
            if (wyr1.oblicz().mode == "wektor" && wyr2.oblicz().mode == "wektor")
                return new Wartosc((wyr1.oblicz()).wektor.odejmij((wyr2.oblicz()).wektor));            
            if (wyr1.oblicz().mode == "macierz" && wyr2.oblicz().mode == "macierz")
                return new Wartosc((wyr1.oblicz()).macierz.odejmij((wyr2.oblicz()).macierz));
            }
        catch(WrongTypeException e){
            throw e;
        }
        catch(Div0Exception e){
            throw e;
        }
        catch(Macierz.WrongSizeException e){
            throw e;
        }
        catch(UndefinedVariableException e){
            throw e;
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        throw new WrongTypeException();
    }
    
    public String opis(){
        return wyr1.opis() + "-" + wyr2.opis();
    }
}
