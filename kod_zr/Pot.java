/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Pot extends operator2arg {
    public Pot(wyrazenie wyr1, wyrazenie wyr2){
        this.wyr1 = wyr1;
        this.wyr2 = wyr2;
    }
    
    public Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException {
        try{
            if (wyr1.oblicz().mode == "rzeczywista" && wyr2.oblicz().mode == "rzeczywista")
                return new Wartosc(Math.pow(wyr1.oblicz().rzeczywista, wyr2.oblicz().rzeczywista));
            
            if (wyr1.oblicz().mode == "macierz" && wyr2.oblicz().mode == "rzeczywista")
                return new Wartosc(wyr1.oblicz().macierz.pow((int) wyr2.oblicz().rzeczywista));
            }
        catch(WrongTypeException e){
            throw e;
        }
        catch(Div0Exception e){
            throw e;
        }
        catch(Macierz.WrongSizeException e){
            throw e;
        }
        catch(UndefinedVariableException e){
            throw e;
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        throw new WrongTypeException();
    }
    
    public String opis(){
        return wyr1.opis() + "*" + wyr2.opis();
    }
}
