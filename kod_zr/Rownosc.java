/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Rownosc extends operator2arg {
    public Rownosc(wyrazenie wyr1, wyrazenie wyr2){
        this.wyr1 = wyr1;
        this.wyr2 = wyr2;
    }
    
    public Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException {
        try{
            if (wyr1 instanceof Zmienna && wyr2 instanceof Zmienna){
                Zmienna x = new Zmienna(wyr1.opis());
                Zmienna y = new Zmienna(wyr2.opis());
                
                try{
                    x.oblicz();
                }
                catch(UndefinedVariableException e){
                    try{
                        y.oblicz();
                        x.przypisz_wart(y.oblicz());        ///w tym przypadku y zdefiniowane, x nie
                        return x.oblicz();
                    }
                    catch(UndefinedVariableException e2){
                        throw e2;                           ///obie zmienne niezdefiniowane
                    }
                }
                try{
                    y.oblicz();
                    x.przypisz_wart(y.oblicz());            ///obie zmienne zdefiniowane
                    return x.oblicz();
                }
                catch(UndefinedVariableException e){
                    y.przypisz_wart(x.oblicz());            ///x zdefiniowane, y nie
                    return x.oblicz();
                }
            }
            
            if (wyr1 instanceof Zmienna){
                Zmienna x = new Zmienna(wyr1.opis());
                x.przypisz_wart(wyr2.oblicz());
                return x.oblicz();
            }
            if (wyr2 instanceof Zmienna){
                Zmienna x = new Zmienna(wyr2.opis());
                x.przypisz_wart(wyr1.oblicz());
                return x.oblicz();
            }

        }
        catch(WrongTypeException e){
            throw e;
        }
        catch(Div0Exception e){
            throw e;
        }
        catch(Macierz.WrongSizeException e){
            throw e;
        }
        catch(UndefinedVariableException e){
            throw e;
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        throw new UndefinedVariableException();
    }
    
    public String opis(){
        return wyr1.opis() + "+" + wyr2.opis();
    }
}
