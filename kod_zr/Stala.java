/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Stala extends wyrazenie {
    Wartosc war;
    
    public Stala(double rzeczywista){
        war = new Wartosc(rzeczywista);
    }
    
    public Stala(Wektor wektor){
        war = new Wartosc(wektor);
    }
    
    public Stala(Macierz macierz){
        war = new Wartosc(macierz);
    }
    
    public Stala(Wartosc war){
        this.war = war;
    }
    
    public Wartosc oblicz() {  
        return war;
    }
    
    public String opis(){
        return war.toString();
    }
}
