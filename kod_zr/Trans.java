/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Trans extends operator1arg {
    public Trans(wyrazenie wyr1){
        this.wyr1 = wyr1;
    }
    
    public Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException {
        try{
            if (wyr1.oblicz().mode == "macierz")
                return new Wartosc(new Macierz(wyr1.oblicz().toString()).transponuj());
        }
        catch(WrongTypeException e){
            throw e;
        }
        catch(Div0Exception e){
            throw e;
        }
        catch(Macierz.WrongSizeException e){
            throw e;
        }
        catch(UndefinedVariableException e){
            throw e;
        }
        catch(Wektor.WrongSizeException e){
            throw e;
        }
        throw new WrongTypeException();
    }
    
    public String opis(){
        return "tr(" + wyr1.opis() + ")";
    }
}
