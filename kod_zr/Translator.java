/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author Jakub
 */
public class Translator {
    Kolekcja_funkcji kol;
    
    public Translator(){
        kol = new Kolekcja_funkcji();
    }

    
    LinkedList<String> scanner(String wejscie) throws SyntaxException {
        if (balance(wejscie) == false)      ///sprawdza poprawnosc postawienia nawiasow klamrowych
            throw new SyntaxException();
        
        LinkedList<String> ciag_wyrazen = new LinkedList<>();
        
        int dl_wej = wejscie.length();
        for (int i = 0; i < dl_wej; i++){
            String wyr = "";
            
            while (charAt(wejscie,0) == ' ' || charAt(wejscie,0) == '\t' || charAt(wejscie,0) == '\n')  ///usuwanie bialych znakow
                wejscie = erase(wejscie,1);
            
            ///liczby
            if (charAt(wejscie,0) >= '0' && charAt(wejscie,0) <= '9'){      
                while ((charAt(wejscie,0) >= '0' && charAt(wejscie,0) <= '9') || 
                        charAt(wejscie,0) == '.'){
                    wyr += charAt(wejscie,0);
                    wejscie = erase(wejscie,1);
                }
                ciag_wyrazen.add(wyr);
                continue;
            }
            
            
            ///operatory i nawiasy
            if (kol.op.get(new String() + charAt(wejscie,0))!=null || //new String() + charAt(wejscie,0) - konwersja char na String
                charAt(wejscie,0) == 40 || charAt(wejscie,0) == 41){ 
                wyr += charAt(wejscie,0);
                wejscie = erase(wejscie,1);
                ciag_wyrazen.add(wyr);
                continue;
            }
            
            ///zmienne i funkcje
            if ((charAt(wejscie,0) >= 'a' && charAt(wejscie,0) <= 'z') 
                || (charAt(wejscie,0) >= 'A' && charAt(wejscie,0) <= 'Z')){      
                while (charAt(wejscie,0) >= 'a' && charAt(wejscie,0) <= 'z' ||
                       charAt(wejscie,0) >= 'A' && charAt(wejscie,0) <= 'Z' ||
                       charAt(wejscie,0) >= '0' && charAt(wejscie,0) <= '9'){
                    wyr += charAt(wejscie,0);
                    wejscie = erase(wejscie,1);
                }
                ciag_wyrazen.add(wyr);
                continue;
            }
            
            ///macierze i wektory
            if (charAt(wejscie,0) == '{'){
                boolean mac = false;
                boolean byl_nawias = false;
                wyr += charAt(wejscie,0);
                wejscie = erase(wejscie,1);
                
                while ((charAt(wejscie,0) != '}' || mac == true) &&
                        (charAt(wejscie,0) != '}' || byl_nawias == false)) {
                    if (wejscie == "")
                        throw new SyntaxException();
                    
                    if (charAt(wejscie,0) == '{'){
                        byl_nawias = false;
                        mac = true;
                    }
                    if (charAt(wejscie,0) == '}')
                        byl_nawias = true;
                    
                    wyr += charAt(wejscie,0);
                    wejscie = erase(wejscie,1);
                }
                
                wyr += charAt(wejscie,0);
                wejscie = erase(wejscie,1);
                ciag_wyrazen.add(wyr);
                continue;
            }
            
            ///przecinek
            if (charAt(wejscie,0) == ','){
                wejscie = erase(wejscie,1);
                wyr += ',';
                ciag_wyrazen.add(wyr);
            }
        }
        
        return ciag_wyrazen;
    }
    
    
    LinkedList<String> negowanie(LinkedList<String> wejscie){ ///zamienia unarny minus na operator negacji !
        LinkedList<String> wyjscie = new LinkedList<>();
        
        boolean byla_wartosc = false;
        for (int i = 0; i < wejscie.size(); i++){
            if (charAt(wejscie.get(i),0) == '-' && byla_wartosc == false){
                wyjscie.add("!");
            }
            else
                wyjscie.add(wejscie.get(i));
            
            if (rozpoznaj(wejscie.get(i)) == "inne")
                byla_wartosc = false;
            else
                byla_wartosc = true;
        }
        
        return wyjscie;
    }
    
    
    LinkedList<String> lexer(LinkedList<String> wejscie) throws SyntaxException {
        LinkedList<String> wyjscie = new LinkedList<>();
        Stack<String> stos = new Stack<>();
        String sym;
        
        try{
            while (wejscie.isEmpty() == false){
                sym = wejscie.pop();
                if (rozpoznaj(sym) == "rzeczywista" || rozpoznaj(sym) == "wektor" ||
                        rozpoznaj(sym) == "macierz" || rozpoznaj(sym) == "zmienna" &&
                        kol.op.containsKey(sym) == false &&  //aby nie pomylic zmiennej z funkcja
                        kol.n_fun2.contains(sym) == false &&
                        kol.n_fun1.contains(sym) == false){
                    wyjscie.add(sym);
                }
                else
                    if (charAt(sym,0) >= 'a' && charAt(sym,0) <= 'z')
                        stos.push(sym);

                if (equal(sym,",")){
                    while (equal(stos.peek(),"(") == false){
                        wyjscie.add(stos.pop());                        
                    }
                }

                if (kol.op.get(sym) != null){
                    while (stos.empty() == false){
                        if (kol.op.get(stos.peek()) == null)            ///jesli element na szczycie nie jest operatorem
                            break;
                        ///operatory o parzystym priorytecie lacza w lewo; o nieparzystym w prawo
                        if (kol.op.get(sym) >= kol.op.get(stos.peek()) && kol.op.get(sym)%2 == 0 ||
                            kol.op.get(sym) > kol.op.get(stos.peek()) && kol.op.get(sym)%2 == 1){
                            if (charAt(sym,0) == '!' && charAt(stos.peek(),0) == '!')   ///jezeli jest podwojna negacja
                                break;
                            wyjscie.add(stos.pop());
                        }
                        else
                            break; ///jesli operator na szczycie ma mniejszy priorytet
                    }
                    stos.push(sym);
                }

                if (equal(sym, "("))
                    stos.push(sym);

                if (equal(sym, ")")){
                    while (equal(stos.peek(), "(")==false) {
                        wyjscie.add(stos.pop());
                    }
                    stos.pop();

                    if (stos.empty() == false)
                        if (charAt(stos.peek(),0) >= 'a' && charAt(stos.peek(),0) <= 'z'){
                            wyjscie.add(stos.pop());
                        }
                }
            }
        }
        catch(EmptyStackException e){
            throw new SyntaxException();
        }
        while (stos.empty() == false){
            if (equal(stos.peek(), "(") || equal(stos.peek(), ")")) 
                throw new SyntaxException();
            
            wyjscie.add(stos.pop());
        }
        return wyjscie;
    }
    
    
    wyrazenie parser(LinkedList<String> wejscie) throws SyntaxException {
        Stack<wyrazenie> stos = new Stack<>();
        try{
            while (wejscie.isEmpty() == false) {
            ///liczby            
                if (rozpoznaj(wejscie.getFirst()) == "rzeczywista")
                    stos.push(new Stala( Double.parseDouble(wejscie.pop() ) ) );
                else{
                    if (rozpoznaj(wejscie.getFirst()) == "macierz")
                        stos.push(new Stala( new Macierz(wejscie.pop() ) ) );
                    else{
                        if (rozpoznaj(wejscie.getFirst()) == "wektor")
                            stos.push(new Stala( new Wektor(wejscie.pop() ) ) );
                        else{
                            if (rozpoznaj(wejscie.getFirst()) == "zmienna" &&   ///jesli dana nazwa nie jest nazwa funkcji, to musi byc nazwa zmiennej
                                    kol.op.containsKey(wejscie.getFirst()) == false &&
                                    kol.n_fun2.contains(wejscie.getFirst()) == false &&
                                    kol.n_fun1.contains(wejscie.getFirst()) == false){
                                stos.push(new Zmienna( wejscie.pop() ));
                            }
                        }
                    }
                }

                if (wejscie.isEmpty())
                    break;


            ///operatory
                if (kol.op.containsKey(wejscie.getFirst()) && charAt(wejscie.getFirst(),0) != '!'){
                    wyrazenie a,b;
                    a = stos.pop();
                    b = stos.pop();
                    stos.push(kol.funkcja2arg(wejscie.pop(), b, a));
                }

                if (wejscie.isEmpty())
                    break;

            ///funkcje2arg
                if (kol.n_fun2.contains(wejscie.getFirst())){
                    wyrazenie a,b;
                    a = stos.pop();
                    b = stos.pop();
                    stos.push(kol.funkcja2arg(wejscie.pop(), a, b));
                }

                if (wejscie.isEmpty())
                    break;

            ///funkcja1arg
                if (kol.n_fun1.contains(wejscie.getFirst()))
                    stos.push(kol.funkcja1arg(wejscie.pop(), stos.pop()));


            }

            return stos.peek();
        
        }
        catch(EmptyStackException e){
            throw new SyntaxException();
        }
    }
    
    
    public wyrazenie kompiluj(String wejscie) throws SyntaxException {
        try{
            return parser(lexer(negowanie(scanner(wejscie))));
        }
        catch(SyntaxException e){
            throw e;
        }
    }
    
    
    class SyntaxException extends Exception {
        public SyntaxException(){
            super();
        }
        
        public String info(){
            return "błąd składni";
        }
    }
    
    /******************* funkcje pomocnicze *************************/
    
    
    String rozpoznaj(String wyrazenie){  //jeszcze dorzucic zmienne
        if (charAt(wyrazenie,0) >= '0' && charAt(wyrazenie,0) <= '9')
            return "rzeczywista";
        
        if (charAt(wyrazenie,0) == '{'){
            for (int i = 1; i < wyrazenie.length(); i++){
                if (charAt(wyrazenie,i) == '{')
                    return "macierz";
            }
            return "wektor";
        }
        
        if (charAt(wyrazenie,0) >= 'a' && charAt(wyrazenie,0) <= 'z' ||
                charAt(wyrazenie,0) >= 'A' && charAt(wyrazenie,0) <= 'Z'){
            for (int i = 1; i < wyrazenie.length(); i++){
                if (charAt(wyrazenie,i) == '(')
                    return "funkcja";
            }
        }
        
        if ((charAt(wyrazenie,0) >= 'a' && charAt(wyrazenie,0) <= 'z' ||
                charAt(wyrazenie,0) >= 'A' && charAt(wyrazenie,0) <= 'Z'))
            return "zmienna";
        
        return "inne";
    }
    
    public char charAt(String napis, int i){
        if (napis == "" || i >= napis.length())
            return 0;
        
        return napis.charAt(i);
    }
    
    boolean balance(String wejscie){ ///sprawdza poprawnosc postawienia nawiasow klamrowych
        int bal = 0;
        for (int i = 0; i < wejscie.length(); i++){
            if (charAt(wejscie,i) == '{')
                bal++;
            
            if (charAt(wejscie,i) == '}')
                bal--;
            
            if (bal < 0)
                return false;
        }
        if (bal == 0)
            return true;
        return false;
    }
    
    String erase(String napis, int i){
        if (i >= napis.length())
            return "";
        
        String nowy = "";
        for (int j = i; j < napis.length(); j++)
            nowy += napis.charAt(j);
        
        return nowy;
    }
    
    boolean equal(String s1, String s2){
        for (int i = 0; i < s1.length() && i < s2.length(); i++){
            if (charAt(s1,i) != charAt(s2,i))
                return false;
        }
        return true;
    }
}
