/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public class Wartosc {
    public double rzeczywista;
    public Wektor wektor;
    public Macierz macierz;
    
    public String mode;
    
    public Wartosc(double rzeczywista){
        this.rzeczywista = rzeczywista;
        mode = "rzeczywista";
    }
    
    public Wartosc(Wektor wektor){
        this.wektor = wektor;
        mode = "wektor";
    }
    
    public Wartosc(Macierz macierz){
        this.macierz = macierz;
        mode = "macierz";
    }
    
    public Wartosc(Wartosc wart){
        this.mode = wart.mode;
        switch(mode){
            case "rzeczywista": this.rzeczywista = wart.rzeczywista;
            case "macierz": this.macierz = wart.macierz;
            case "wektor": this.wektor = wart.wektor;
        }
    }
    
    private String double_toString(double licz){
        String liczba = String.valueOf(licz);
        if (liczba == null)
            return "0";
        if (liczba.endsWith(".0")){
            if (licz == -0.0)
                return "0";
            
            return liczba.substring(0, liczba.length()-2);
        }
        else return liczba;
    }
    
    public String toString(){
        if (mode == "rzeczywista")
            return double_toString(rzeczywista);
        if (mode == "wektor")
            return wektor.toString();
        
        return macierz.toString();
    }
}
