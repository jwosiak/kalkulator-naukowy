/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

import java.util.LinkedList;

/**
 *
 * @author Jakub
 */

public class Wektor {
    public double wsp[];
    
    public Wektor(double wsp[]){
        this.wsp = wsp;
    }
    
    private LinkedList<String> scanner (String wejscie){
        LinkedList<String> liczby = new LinkedList<>();
        for (int i = 0; i < wejscie.length(); i++){
            String liczba = "";
            for (int j = i; (wejscie.charAt(j) >= '0' && wejscie.charAt(j) <= '9') || wejscie.charAt(j) == '.' || wejscie.charAt(j) == '-'; j++, i++){
                liczba += wejscie.charAt(j);
            }
            if (liczba != "")
                liczby.add(liczba);
        }
        return liczby;
    }
    
    public Wektor(String wek){
        LinkedList<String> liczby = scanner(wek);
        double wsp[] = new double[liczby.size()];
        for (int i = 0; i < liczby.size(); i++){
            wsp[i] = Double.valueOf(liczby.get(i));
        }
        this.wsp = wsp;
    }
    
    public String toString(){
        String napis = new String();
        
        if (wsp.length == 0)
            return "";
        
        napis += "{";
        for (int i = 0; i < wsp.length; i++){
            napis += double_toString(wsp[i]);
            if (i != wsp.length-1)
                napis += ", ";
        }
        napis += "}";
        return napis;
    }
    
    public Wektor dodaj(Wektor w2) throws WrongSizeException {
        if (wsp.length != w2.wsp.length)
            throw new WrongSizeException();
        double wsp[] = new double[this.wsp.length];
        for (int i = 0; i < wsp.length; i++){
            wsp[i] = this.wsp[i] + w2.wsp[i];
        }
        
        Wektor w3 = new Wektor(wsp);
        return w3;
    }
    
    public Wektor mnoz(double skalar){
        double wsp[] = new double[this.wsp.length];
        for (int i = 0; i < wsp.length; i++){
            wsp[i] = this.wsp[i] * skalar;
        }
        
        Wektor w3 = new Wektor(wsp);
        return w3;
    }
    
    public double mnoz(Wektor w2) throws WrongSizeException { ///iloczyn skalarny
        if (wsp.length != w2.wsp.length)
            throw new WrongSizeException();
        
        double wynik = 0;
        for (int i = 0; i < wsp.length; i++)
            wynik += wsp[i]*w2.wsp[i];
        
        return wynik;        
    }
    
    public Wektor odejmij(Wektor w2) throws WrongSizeException {
        if (wsp.length != w2.wsp.length)
            throw new WrongSizeException();
        return this.dodaj(w2.mnoz(-1));
    }
    
    private String double_toString(double licz){
        String liczba = String.valueOf(licz);
        if (liczba == null)
            return "0";
        if (liczba.endsWith(".0")){
            if (licz == -0.0)
                return "0";
            return liczba.substring(0, liczba.length()-2);
        }
        else return liczba;
    }
    
    class WrongSizeException extends Exception{
        public WrongSizeException(){
            super();
        }
        
        public String info(){
            return "nieprawidłowy rozmiar wektora";
        }
    }
}

