/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 *
 * @author Jakub
 */
public class Zmienna extends wyrazenie {
    private String nazwa;
    private static Hashtable<String,Wartosc> lista_zmiennych = new Hashtable<>();
    
    public Zmienna(String nazwa){
        this.nazwa = nazwa;
    }
    
    public void przypisz_wart(Wartosc wart){
        lista_zmiennych.put(nazwa, wart);
    }
    
    public Wartosc usun_zm(){
        return lista_zmiennych.remove(nazwa);
    }
    
    public Wartosc oblicz() throws UndefinedVariableException {
        if (lista_zmiennych.get(nazwa) != null)
            return lista_zmiennych.get(nazwa);
        else
            throw new UndefinedVariableException();
    }
    
    public String opis(){
        return nazwa;
    }
    
    public String wszystkie_zmienne(){
        String wyjscie = "";
        ConcurrentSkipListSet<String> zm = new ConcurrentSkipListSet<>();
        zm.addAll(lista_zmiennych.keySet());
        while(zm.isEmpty() == false){
            wyjscie += zm.pollFirst();            
            if (zm.isEmpty() == false)
                wyjscie += ", ";
        }
        
        return wyjscie;
    }
}



/*  stary kod
public void przypisz_wart(Wartosc wart){
        if (nazwa.charAt(0) != '-')
            lista_zmiennych.put(nazwa, wart);
        else
            lista_zmiennych.put(erase(nazwa,1), wart);
    }


public Wartosc oblicz() throws WrongTypeException {
        if (nazwa.charAt(0) != '-')
            return lista_zmiennych.get(nazwa);
        
        try{
            return new Negacja(new Stala(lista_zmiennych.get(erase(nazwa,1)))).oblicz();
        }
        catch(WrongTypeException e){
            throw e;
        }
    }
*/
