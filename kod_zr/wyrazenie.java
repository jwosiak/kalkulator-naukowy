/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt;

/**
 *
 * @author Jakub
 */
public abstract class wyrazenie {
    public abstract Wartosc oblicz() throws WrongTypeException, Div0Exception, Macierz.WrongSizeException,
            UndefinedVariableException, Wektor.WrongSizeException;
    public abstract String opis();
    
    class WrongTypeException extends Exception {
        public WrongTypeException(){
            super();
        }
        
        public String info(){
            return "błąd typu";
        }
    }
    
    class Div0Exception extends Exception{
        public Div0Exception(){
            super();
        }
        
        public String info(){
            return "nie można dzielić przez 0, oraz przez macierz o wyznaczniku 0";
        }
    }
    
    class UndefinedVariableException extends Exception {
        public UndefinedVariableException(){
            super();
        }
        
        public String info(){
            return "zmienna o niezdefiniowanej wartości";
        }
    }
}
